use std::fs::File;
use std::io::Write;
use std::str::FromStr;
use std::sync::{Mutex, OnceLock};

#[allow(dead_code)]
pub fn write(msg: &str) {
    let mut lock = logger().lock().unwrap();
    lock.buffer.push_str(msg);
    lock.buffer.push('\n');
}

pub fn flush() {
    let log = logger().lock().unwrap();
    let mut output = File::create(&log.path).unwrap();
    _ = write!(output, "{}", &log.buffer);
}

fn logger() -> &'static Mutex<Logger> {
    static LOGGER: OnceLock<Mutex<Logger>> = OnceLock::new();
    LOGGER.get_or_init(|| Mutex::new(Logger::new()))
}

struct Logger {
    buffer: String,
    path: String,
}

impl Logger {
    fn new() -> Self {
        let buffer = String::with_capacity(4096);
        let path = String::from_str("/run/init.log").unwrap();

        Self { buffer, path }
    }
}
