use crate::helpers::cmd;
pub mod log;

pub fn findfs_by_suffix(part_suffix: &str) -> Option<String> {
    let devices = cmd::blkid();
    for line in devices.split('\n') {
        let dev = line.split(' ').next().unwrap();
        let dev = &dev[..&dev.len() - 1]; //remove the :
        if dev.ends_with(part_suffix) {
            return Some(dev.to_owned());
        }
    }
    None
}

pub fn get_first_luks_device() -> Option<String> {
    let expected = "TYPE=\"crypto_LUKS\"";
    for line in cmd::blkid().split('\n') {
        if line.contains(expected) {
            let device = line.split(':').next().unwrap();
            return Some(device.to_string());
        }
    }
    None
}
