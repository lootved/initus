pub mod cmd;
pub mod serializer;
use crate::config::Config;
use crate::fs::findfs_by_suffix;
use cmd::*;

#[allow(unused)]
pub fn is_ext4(path: &str) -> bool {
    fs_type(path) == "ext4"
}

fn fs_type(path: &str) -> String {
    // will not proceed if command fails
    let args = ["-o", "value", "-s", "TYPE", path];
    let mut raw_res = run("blkid", &args);
    // remove newline
    raw_res.pop();
    raw_res
}

pub fn losetup_disk(disk: &str) -> String {
    let args = ["-f"];
    let lodev = losetup(&args);
    let args = ["-P", &lodev, disk];
    losetup(&args);
    lodev
}

pub fn mount_base_fs() {
    mkdirs("/proc /sys /dev /run /mnt/root");

    let mnter = SafeMounter::new();
    mnter.mount("sysfs", "nodev,noexec,nosuid", "sysfs", "/sys");
    mnter.mount("proc", "nodev,noexec,nosuid", "proc", "/proc");
    mnter.mount("devtmpfs", "nosuid,mode=0755", "udev", "/dev");

    mkdirs("/dev/pts");

    mnter.mount(
        "devpts",
        "noexec,nosuid,gid=5,mode=0620",
        "devpts",
        "/dev/pts",
    );

    mnter.mount(
        "tmpfs",
        "nodev,noexec,nosuid,size=10%,mode=0755",
        "tmpfs",
        "/run",
    );

    ln_s("/proc/self/fd", "/dev/fd");
    ln_s("/proc/self/fd/0", "/dev/stdin");
    ln_s("/proc/self/fd/1", "/dev/stdout");
    ln_s("/proc/self/fd/2", "/dev/stderr");
}

pub fn load_drivers() {
    // should retrieve modules from a file
    let args = ["-a", "nvme", "dm_mod", "dm_crypt", "loop"];
    run("modprobe", &args);

    std::env::set_var("SYSTEMD_LOG_LEVEL", "notice");

    // start udev to manage external devices like usb keyboard
    let args = ["--daemon", "--resolve-names=never"];
    run("/lib/systemd/systemd-udevd", &args);

    let args = ["trigger", "--type=subsystems", "--action=add"];
    run_without_check("udevadm", &args);

    let args = ["trigger", "--type=devices", "--action=add"];
    run_without_check("udevadm", &args);
    // wait for udev to be ready
    let args = ["settle", "--timeout=10"];
    run("udevadm", &args);
}

pub fn activate_lvm(device: &str, maybe_key_file: Option<&str>) {
    let suffix = device.split('/').nth(2).unwrap();
    let name = format!("{suffix}_crypt");
    let mut args = vec!["luksOpen", device, &name];
    if let Some(key_file) = maybe_key_file {
        args.push("--key-file");
        args.push(key_file);
    } else {
        eprint!("\nPlease unlock disk {name}: ");
    }

    run_with_display("cryptsetup", &args);
    lvm_scan();
}

pub fn mount_root(device: &str, ro: bool) {
    // TODO retrieve fs type automatically
    mount_ext4(ro, device, "/mnt/root")
}

pub fn pre_switch(cfg: &Config, ro: bool, pwd: &str) {
    let data_path = "/run/data";
    mkdirs(data_path);

    let kf = "/etc/kf";
    if !cmd::openssl("/etc/kf.b64", kf, pwd) {
        exit!("unvalid kf config");
    }

    if let Some(datafs) = findfs_by_suffix(&cfg.data_suffix) {
        eprintln!();
        mount_ext4(ro, &datafs, data_path);

        let args = ["luksOpen", &cfg.home_img, "dh", "-d", kf];
        run("cryptsetup", &args);
        let dst = "/run/data/h";
        mkdirs(dst);
        mount_ext4(ro, "/dev/mapper/dh", dst);
    }
    eprintln!();
}

pub fn decrypt_file(inpt: &str, out: &str) -> Option<String> {
    for _ in 0..3 {
        let pass = askpass();
        if openssl(inpt, out, &pass) {
            return Some(pass);
        }
    }
    None
}

pub fn decrypt_lvm(device: &str, key_file: &str) -> String {
    let dec_file = "/etc/kc";
    let pwd = decrypt_file(key_file, dec_file).expect("something went wrong");
    activate_lvm(device, Some(dec_file));
    pwd
}

pub fn prep_switch_to_usr_system() {
    // TODO: setup overlay when ro
    mkdirs("/mnt/root/run /mnt/root/sys /mnt/root/proc /mnt/root/dev");
    let dirs = ["/run", "/sys", "/proc", "/dev"];
    for src in dirs {
        let dst = format!("/mnt/root{src}");
        mount_move(src, &dst);
    }
}

macro_rules! exit {
    ($($arg:tt)*) => {{
        eprintln!($($arg)*);
        std::process::exit(1);
    }}
}

macro_rules! dlog {
    ($($x:tt)*) => {
        {
            #[cfg(debug_assertions)]
            {
                std::dbg!($($x)*)
            }
            #[cfg(not(debug_assertions))]
            {
                //($($x)*)
                ()
            }
        }
    }
}

pub(crate) use dlog;
pub(crate) use exit;
