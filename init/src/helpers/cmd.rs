use super::exit;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::process::{Command, Output, Stdio};
pub fn losetup(args: &[&str]) -> String {
    let mut res = run("losetup", args);
    res.pop(); // get rid of newline
    res
}

pub fn mount(fs_type: &str, opts: &str, src: &str, dst: &str) {
    let args = ["-t", fs_type, "-o", opts, src, dst];
    run("mount", &args);
}

pub fn mount_ext4(ro: bool, src: &str, dst: &str) {
    let opts = if ro {
        "nodiratime,noatime,ro"
    } else {
        "nodiratime,noatime,rw"
    };
    mount("ext4", opts, src, dst);
}

pub fn askpass() -> String {
    let args = [" "];
    run("/usr/lib/cryptsetup/askpass", &args)
}

pub struct SafeMounter {
    mounted: HashSet<String>,
}

impl SafeMounter {
    //avoid error due to remounting
    pub fn new() -> Self {
        let mounted = get_mounted_folders();

        Self { mounted }
    }
    pub fn mount(&self, fs_type: &str, opts: &str, src: &str, dst: &str) {
        // allows running from the shell of another init
        // that stops the boot process at initramfs
        // mainly used for debugging via ssh
        if !self.mounted.contains(dst) {
            mount(fs_type, opts, src, dst);
        }
    }
}

pub fn mkdirs(paths: &str) {
    let mut args: Vec<&str> = Vec::with_capacity(10);
    args.push("-p");
    args.extend(paths.split(' '));
    run("mkdir", &args);
}

pub fn mount_move(src: &str, dst: &str) {
    let args = ["-n", "-o", "move", src, dst];
    run("mount", &args);
}

pub fn lvm_scan() {
    let args = ["vgscan", "--mknodes"];
    run("lvm", &args);

    let args = ["vgchange", "--sysinit", "-a", "ly"];
    run("lvm", &args);

    let args = ["vgscan", "--mknodes"];
    run("lvm", &args);
}

pub fn findfs(device: &str) -> String {
    let args = [device];
    let mut res = run("findfs", &args);
    // remove the \n
    res.pop();
    res
}

pub fn blkid() -> String {
    let args = [];
    run("blkid", &args)
}

pub fn ln_s(src: &str, dst: &str) {
    // avoid failure by running only if dst does not exist
    let args = ["-s", src, dst];
    if !std::path::Path::new(dst).exists() {
        run("ln", &args);
    }
}

#[allow(dead_code)]
pub fn switch_root() {
    // not working
    use std::os::unix::process::CommandExt;
    let args = ["/mnt/root", "/sbin/init"];

    let err = Command::new("switch_root").args(args).exec();
    let msg = format!("unable to switch_root: {err}");
    drop_to_shell(&msg);
}

pub fn drop_to_shell(msg: &str) {
    use std::os::unix::process::CommandExt;
    println!("{RED_COLOR}failure while running: {msg}{NO_COLOR}");
    let args = ["cttyhack", "/bin/sh"];

    let err = Command::new("setsid").args(args).exec();
    println!("Error trying to open shell: {}", err);
    println!("will sleep for 20 seconds then exit ...");
    sleepms(20_000);
    std::process::exit(1);
}

pub fn run(cmd: &str, args: &[&str]) -> String {
    let res = _run(cmd, args, false);
    if res.status.success() {
        std::str::from_utf8(&res.stdout).unwrap().to_owned()
    } else {
        let stdout = std::str::from_utf8(&res.stdout).unwrap();
        let stderr = std::str::from_utf8(&res.stderr).unwrap();

        let msg = format!("{cmd} {args:?}\nstdout: {stdout}\nstderr: {stderr}");
        drop_to_shell(&msg);
        unreachable!("would either exec or exit 1");
    }
}

pub fn run_with_display(cmd: &str, args: &[&str]) {
    let res = _run(cmd, args, false);

    if !res.status.success() {
        let msg = format!("{cmd} {args:?}");
        drop_to_shell(&msg);
    }
}

pub fn run_without_check(cmd: &str, args: &[&str]) {
    _ = _run(cmd, args, false)
}

pub fn openssl(inpt: &str, out: &str, pwd: &str) -> bool {
    let pass = format!("pass:{pwd}");
    let args = [
        "enc",
        "-aes-256-cbc",
        "-md",
        "sha512",
        "-pbkdf2",
        "-nosalt",
        "-a",
        "-in",
        inpt,
        "-out",
        out,
        "-d",
        "-pass",
        &pass,
    ];
    _run("openssl", &args, false).status.success()
}

pub fn sleepms(ms: u64) {
    use std::{thread, time};
    let wait_time = time::Duration::from_millis(ms);
    thread::sleep(wait_time);
}

pub fn read_lines(path: &str) -> io::Lines<BufReader<File>> {
    let file = File::open(path).expect("path {path} does not exist");
    io::BufReader::new(file).lines()
}

fn get_mounted_folders() -> HashSet<String> {
    let mtab_path = "/proc/mounts";
    if std::path::Path::new(mtab_path).exists() {
        read_lines(mtab_path)
            .map(|line| {
                let line = line.unwrap();
                line.split(' ').nth(1).unwrap().to_owned()
            })
            .collect()
    } else {
        HashSet::new()
    }
}

fn _run(cmd: &str, args: &[&str], display: bool) -> Output {
    if cfg!(feature = "trace") || cfg!(debug_assertions) {
        eprintln!("running: {cmd} {args:?}");
        sleepms(150);
    }

    let (stdout, stderr) = if display {
        (Stdio::inherit(), Stdio::inherit())
    } else {
        (Stdio::piped(), Stdio::piped())
        //(Stdio::null(), Stdio::null())
    };

    Command::new(cmd)
        .args(args)
        .stdout(stdout)
        .stderr(stderr)
        .stdin(Stdio::inherit())
        .spawn()
        .unwrap_or_else(|_| exit!("unable to run {}", cmd))
        .wait_with_output()
        .unwrap()
}

const RED_COLOR: &str = "\x1b[31;1m";
const NO_COLOR: &str = "\x1b[0m";
