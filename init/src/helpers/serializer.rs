struct Serializer {
    o: [u8; 10],
    e: [u8; 10],
}

pub fn read(path: &str) -> Vec<String> {
    let encoded = std::fs::read(path).expect("unable to read init config");
    let s = Serializer::new();
    s.decode(&encoded)
}

pub fn write(cfg: &[&str], path: &str) {
    let s = Serializer::new();
    let encoded = s.encode(cfg);
    std::fs::write(path, encoded).expect("unable to save config");
}

impl Serializer {
    fn new() -> Self {
        Self {
            o: [7, 9, 38, 5, 18, 23, 31, 17, 11, 9],
            e: [37, 21, 27, 17, 3, 42, 33, 7, 9, 11],
        }
    }

    fn decode(&self, bytes: &[u8]) -> Vec<String> {
        let mut res = Vec::with_capacity(10);

        let mut buff: Vec<u8> = Vec::with_capacity(100);
        let mut val: u8;
        let mut p = self.e[0] - self.o[1];
        let mut s = 0;
        let mut i = 0;
        while i < bytes.len() - 1 {
            while bytes[i] != self.e[s] {
                val = bytes[i] + self.o[s] - self.o[s + 3];
                val = ((val ^ p) ^ (p ^ self.o[s + 2])) - self.o[s];
                buff.push(val);
                p = bytes[i] + self.o[s] - self.o[s + 3];
                i += 1;
            }
            let extracted = std::str::from_utf8(&buff).unwrap().to_owned();
            buff.clear();
            s += 1;
            i += 1;
            res.push(extracted);
        }
        res
    }

    fn encode(&self, vals: &[&str]) -> Vec<u8> {
        let mut res = Vec::with_capacity(200);

        let mut i = 0;
        let mut p = self.e[0] - self.o[1];
        while i < vals.len() {
            for b in vals[i].bytes() {
                let val = ((b + self.o[i]) ^ (p ^ self.o[i + 2])) ^ p;
                res.push(val + self.o[i + 3] - self.o[i]);
                p = val;
            }
            res.push(self.e[i]);
            i += 1;
        }
        res
    }
}
