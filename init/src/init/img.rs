use crate::cmd;
use crate::config;

use crate::helpers::*;

use crate::args;
use crate::fs;

pub fn img(_opts: &args::Opts) {
    let crypt_dev = fs::get_first_luks_device().unwrap();
    let config_path = std::env::var("_INITFS_CONF_PATH_").unwrap_or("/etc/kmap".to_string());
    dlog!("parsing config");
    let cfg = config::read(&config_path);
    dlog!("parsed cfg:", &cfg);
    let pwd = decrypt_lvm(&crypt_dev, "/etc/kc.b64");

    pre_switch(&cfg, false, &pwd);
    cmd::sleepms(100);

    losetup_disk(&cfg.root_img);
    let root_path = cmd::findfs("LABEL=root");

    mount_root(&root_path, false);

    prep_switch_to_usr_system();
}
