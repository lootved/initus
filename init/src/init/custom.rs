use crate::args;
use crate::config;
use crate::fs;
use crate::helpers::*;

pub fn custom(opts: &args::Opts) {
    let crypt_dev = fs::get_first_luks_device().unwrap();
    let config_path = std::env::var("_INITFS_CONF_PATH_").unwrap_or("/etc/kmap".to_string());
    let cfg = config::read(&config_path);

    let pwd = decrypt_lvm(&crypt_dev, "/etc/kc.b64");

    let root_path =
        fs::findfs_by_suffix(&cfg.root_suffix).expect("unable to find rootfs via suffix");

    mount_root(&root_path, opts.ro);

    pre_switch(&cfg, opts.ro, &pwd);

    prep_switch_to_usr_system();
    // unreachable!("ERROR-- system was unable to switch_root");
}
