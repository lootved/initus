use crate::args;
use crate::fs;
use crate::helpers::*;

pub fn autodetect(opts: &args::Opts) {
    let root_path = if let Some(path) = fs::get_first_luks_device() {
        activate_lvm(&path, None);
        fs::findfs_by_suffix("root").expect("unable to find rootfs via suffix")
    } else {
        cmd::findfs("LABEL=root")
    };

    mount_root(&root_path, opts.ro);
    prep_switch_to_usr_system();
}
