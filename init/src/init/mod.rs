mod autodetect;
mod custom;
mod img;

pub use autodetect::*;
pub use custom::*;
pub use img::*;

// TODO: overlay before switch root prep
// mount root to /run/over/low as ro
// mount -t overlay -o lowerdir=/run/over/low,upperdir=/run/over/up,workdir=/run/over/work /mnt/root
