use crate::helpers::serializer;

pub fn read(path: &str) -> Config {
    let (root_img, home_img, data_suffix, root_suffix) = if std::path::Path::new(path).exists() {
        let mut encoded = serializer::read(path);
        (
            encoded.pop().unwrap(),
            encoded.pop().unwrap(),
            encoded.pop().unwrap(),
            encoded.pop().unwrap(),
        )
    } else {
        (
            "root".to_owned(),
            "home".to_owned(),
            "data".to_owned(),
            "root".to_owned(),
        )
    };
    Config {
        root_suffix,
        data_suffix,
        home_img,
        root_img,
    }
}

pub fn write(cfg: &Config, path: &str) {
    let vals = [
        cfg.root_suffix.as_ref(),
        cfg.data_suffix.as_ref(),
        cfg.home_img.as_ref(),
        cfg.root_img.as_ref(),
    ];
    serializer::write(&vals, path);
}

#[derive(Debug)]
pub struct Config {
    pub root_suffix: String,
    pub data_suffix: String,
    pub home_img: String,
    pub root_img: String,
}
