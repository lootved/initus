pub struct Opts {
    pub ro: bool,
    pub autodetect: bool,
    pub img: bool,
}

pub fn parse() -> Opts {
    let Ok(args) = std::fs::read_to_string("/proc/cmdline") 
     else {
        std::process::exit(1);
    };

    let mut ro = false;
    let mut autodetect = false;
    let mut img = false;
    //TODO: handle args with space when needed
    for arg in args.split(' ') {
        match arg {
            "ro" => ro = true,
            "autodetect" => autodetect = true,
            "boot=img" => img = true,
            _ => (),
        }
    }

    Opts {
        ro,
        autodetect,
        img,
    }
}
