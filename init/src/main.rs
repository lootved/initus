mod args;
mod config;
mod fs;
mod helpers;
mod init;
use crate::helpers::*;

fn main() {
    // generate_config();
    // read_config();

    mount_base_fs();
    load_drivers();

    dlog!("setting up system..");
    let opts = args::parse();
    if opts.img {
        init::img(&opts);
    } else if opts.autodetect {
        init::autodetect(&opts);
    } else {
        init::custom(&opts);
    }
    fs::log::flush();
}

#[allow(dead_code)]
fn read_config() {
    let path = std::env::var("_INITFS_CONF_PATH_").unwrap_or("../target/config".to_string());
    let cfg = config::read(&path);
    print!("{path}: {cfg:?}");
    exit!("stop");
}

#[allow(dead_code)]
fn generate_config() {
    let path = "../target/config";
    let root_suffix = "root".to_string();
    let data_suffix = "data".to_string();
    let home_img = "".to_string();
    let root_img = "".to_string();

    let cfg = config::Config {
        root_suffix,
        data_suffix,
        home_img,
        root_img,
    };
    println!("original: {cfg:?}");

    config::write(&cfg, path);

    let cfg = config::read(path);
    println!("new: {cfg:?}");
    exit!("stop");
}
