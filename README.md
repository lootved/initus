# initus

Early init program for linux on x86-64.

Build initramfs and the init of the initramfs for linux.

Uses systemd-udev as it used by debian, and the tool is developed
on debian.

# compile the project
 ```sh
    cargo build --release --target x86_64-unknown-linux-musl
 ```
 optional feature  trace

# prepare an initramfs
could either be done via the 'initus' binary or `cargo run`.
path is relative to the root of the workspace


# Dependencies
    dependencies used to build on debian
    `pax-utils` provides lddree to identify shared lib dependences
    `file` provides file to identify mimetype
    `lz4` provides lz4 for fast compression
    `upx` optional to compress the binary
    `console-data` optional to select a keymap

# Objectives
- fast initramfs generation
- minimal kernel modules inclusion based on the system
- fastish compilation of the init


# Lint
``` cargo clippy --fix  --allow-staged --allow-dirty ```

