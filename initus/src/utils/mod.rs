pub mod sh;

pub fn build_initramfs(initus_path: &str, initramfs_dir: &str) {
    let to_copy = ["/usr/bin/openssl", "/lib/x86_64-linux-gnu/libssl.so.3"];
    for src in to_copy.iter() {
        let dst = format!("{initramfs_dir}{src}");
        copy_file(src, &dst);
    }
    copy_initus(initus_path, initramfs_dir);
}

pub fn package_initramfs(initramfs_dir: &str, microcode_path: &str, output: &str) {
    assert!(std::env::set_current_dir(initramfs_dir).is_ok());
    let initrd = "/tmp/_initrd";
    make_cpio(initramfs_dir, initrd);
    cmd!("zstd -f {initrd} -o {initrd}.zstd");
    cmd_no_display!("cat {microcode_path} {initrd}.zstd | sudo tee {output} ");
}

pub fn make_cpio(base_dir: &str, output: &str) {
    assert!(std::env::set_current_dir(base_dir).is_ok());
    cmd_no_display!("find . -print0 | cpio --null -o --format=newc -F {output}");
}

fn generate_init_file(init_path: &str) {
    if !std::path::Path::new(init_path).exists() {
        let init = "#!/bin/sh
# doing switch_root inside the binary
# putting /initsetup in /init
# and exec /initsetup  inside script
# does not work
/initsetup
exec switch_root /mnt/root /sbin/init
";
        std::fs::write(init_path, init).expect("unable to save config");
    }
}
pub fn copy_file(src: &str, dst: &str) {
    if !std::path::Path::new(dst).exists() {
        std::fs::copy(src, dst).unwrap();
    }
}
fn copy_initus(initus_path: &str, initramfs_dir: &str) {
    let init_path = "target/init";
    generate_init_file(init_path);
    println!("copy {initus_path} to {initramfs_dir}/initsetup");
    cmd!("chmod 755 {init_path} {initus_path}");
    cmd!("rsync {initus_path} {initramfs_dir}/initsetup");
    cmd!("rsync {init_path} {initramfs_dir}/init");
}
