#![macro_use]
use std::process::{Child, Command, Output, Stdio};

#[macro_export]
macro_rules! cmd {
    ( $($arg:tt)*) => {
        _cmd_!(true, true,  $($arg)*)
    };
}

#[macro_export]
macro_rules! cmd_can_fail {
    ( $($arg:tt)*) => {
        _cmd_!(false, true,  $($arg)*)
    };
}

#[macro_export]
macro_rules! cmd_no_display {
    ( $($arg:tt)*) => {
        _cmd_!(true, false,  $($arg)*)
    };
}

#[macro_export]
macro_rules! _cmd_ {
    ( $failure:expr, $display:expr, $($arg:tt)*) => {
        let lines = format!($($arg)*);

        let commands: Vec<&str> = lines.split("\n").filter(|s| !s.trim().is_empty()).collect();
        for command in commands {
            let res = crate::utils::sh::run_cmd_line(command, $display);
            if $failure && ! res.status.success() {
                    println!("previous command failed: {command} ");
                    println!("{res:?}");
                    std::process::exit(1);
                    }
            }
    }
}

pub fn run_cmd_line(line: &str, display: bool) -> Output {
    let mut cmds: Vec<_> = line.split('|').collect();

    let first_cmd = cmds.remove(0);

    let mut args: Vec<_> = first_cmd.split(' ').filter(|s| !s.is_empty()).collect();
    //eprintln!("args before: {:?}", args);
    let cmd = args.remove(0);

    //if it's a single command run and exit
    if cmds.is_empty() {
        return _run(cmd, &args, display, None).wait_with_output().unwrap();
    }
    // if chain of commands run all intermediate cmds with no display
    let mut child = _run(cmd, &args, false, None);
    // will not do anything if it's a single command
    let mut cmd_idx = 0;
    while cmd_idx < cmds.len() - 1 {
        let line = cmds[cmd_idx];
        let mut args: Vec<_> = line.split(' ').filter(|s| !s.is_empty()).collect();
        let cmd = args.remove(0);

        let previous_stdout = child.stdout.unwrap();
        //
        let new_child = _run(cmd, &args, false, Some(previous_stdout.into()));
        child = new_child;
        cmd_idx += 1;
    }

    let line = cmds[cmd_idx];
    let mut args: Vec<_> = line.split(' ').filter(|s| !s.is_empty()).collect();
    let cmd = args.remove(0);
    _run(cmd, &args, display, Some(child.stdout.unwrap().into()))
        .wait_with_output()
        .unwrap()
}

#[allow(dead_code)]
pub fn run(cmd: &str, args: &[&str]) -> Output {
    _run(cmd, args, false, None).wait_with_output().unwrap()
}

fn _run(cmd: &str, args: &[&str], display: bool, stdin: Option<Stdio>) -> Child {
    let (stdout, stderr) = if display {
        (Stdio::inherit(), Stdio::inherit())
    } else {
        (Stdio::piped(), Stdio::piped())
    };
    let stdin = if let Some(val) = stdin {
        val
    } else {
        Stdio::inherit()
    };

    // eprintln!("{} : {:?}", cmd, args);
    Command::new(cmd)
        .args(args)
        .stdout(stdout)
        .stderr(stderr)
        .stdin(stdin)
        .spawn()
        .unwrap()
}
