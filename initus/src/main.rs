mod utils;

fn main() {
    // rootfs MUST be owned by root when creating an initramfs
    let initramfs_base_dir = "/usr/src/initramfs";
    if std::path::Path::new("../Cargo.lock").exists() {
        // make sure program is run from repos base
        // when running from cargo.
        // should not produce any weird side effect
        assert!(std::env::set_current_dir("..").is_ok());
    }
    let project_dir_tmp = std::env::current_dir().unwrap();
    let project_dir = project_dir_tmp.to_str().unwrap();

    let initus_path = if std::env::args().nth(1).is_none() {
        "target/debug/init"
    } else {
        "target/release/init"
    };

    let version = "2";
    let initramfs_ouput = format!("/mnt/initrd.img{version}");

    let initramfs_version_dir = format!("{initramfs_base_dir}/{version}");
    let initramfs_dir = format!("{initramfs_version_dir}/main");

    if !std::path::Path::new(&initramfs_dir).exists() {
        // base initramfs is currently taken from a manually built image
        // TODO automate building the initial base image
        std::fs::create_dir_all(&initramfs_version_dir).unwrap();
        //  to generate the initr
        let new_kernel_version = "6.1.0-16-amd64";
        let sys_initrd = format!("/boot/initrd.img-{new_kernel_version}");
        if !std::path::Path::new(&sys_initrd).exists() {
            panic!("system initrd does not exist {sys_initrd},\ngenerate it with update-initramfs -c -k {new_kernel_version}");
        }
        cmd!("unmkinitramfs {sys_initrd} {}", &initramfs_version_dir);
        // manually copy custom config
        for file in ["kc.b64", "kf.b64", "kmap"] {
            let dst = format!("{initramfs_dir}/etc/{file}");
            let src = format!("{initramfs_base_dir}/1/main/etc/{file}");
            utils::copy_file(&src, &dst);
        }
    }

    let microcode_cpio = format!("{initramfs_base_dir}/microcode.cpio");
    if !std::path::Path::new(&microcode_cpio).exists() {
        let early_dir = format!("{initramfs_version_dir}/early");
        utils::make_cpio(&early_dir, &microcode_cpio);
    }

    assert!(std::env::set_current_dir(project_dir).is_ok());
    utils::build_initramfs(initus_path, &initramfs_dir);

    for file in ["kc.b64", "kf.b64", "kmap"] {
        let src = format!("{initramfs_dir}/etc/{file}");
        if !std::path::Path::new(&src).exists() {
            panic!("one of the config files kc.b64,kf.b64,kmap is missing!")
        }
    }

    mount_boot_if_needed();
    utils::package_initramfs(&initramfs_dir, &microcode_cpio, &initramfs_ouput);
}

fn mount_boot_if_needed() {
    // check that something is mounted in /mnt assuming it's the boot partition
    // otherwise mount the boot partition to /mnt
    use std::io::{BufRead, BufReader};
    let f = std::fs::File::open("/etc/mtab").expect("unable to read mtab");
    let reader = BufReader::new(f);

    for line in reader.lines() {
        let line = line.unwrap();
        let mut word_it = line.split(' ');
        _ = word_it.next(); //ignore first word
        let mnt_point = word_it.next().expect("mtab was malformed");
        if mnt_point == "/mnt" {
            return;
        }
    }
    let _boot_device_path =
        std::env::var("_BOOT_DEVICE_PATH").expect("_BOOT_DEVICE_PATH must be defined");
    cmd!("sudo mount -o noatime {_boot_device_path} /mnt");
}
